import jwt from "jsonwebtoken";

const createToken = (data) => {
  let token = jwt.sign({ data }, "capstone_express", { expiresIn: "5y" });
  return token;
};

const checkToken = (token) => {
  return jwt.verify(token, "capstone_express");
};

const decodeToken = (token) => {
  return jwt.decode(token);
};

export const khoaApi = (req, res, next) => {
  let { token } = req.headers;
  if (token) {
    if (checkToken(token)) {
      decodeToken(token);
    } else {
      res.status(401).send("Token không hợp lệ!");
    }
    next(); //bypass
  } 
  else res.status(401).send("Không có quyền truy cập!");
}

export { createToken, checkToken, decodeToken };
