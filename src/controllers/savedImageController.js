import { PrismaClient } from "@prisma/client";
import { decodeToken } from "../config/jwt.js";

const prisma = new PrismaClient();

const getListSavedImageById = async (req, res) => {
  let { hinh_id } = req.params;
  try {
    let savedImage = await prisma.luu_anh.findFirst({
      where: {
        hinh_id: Number(hinh_id),
      },
      include: {
        hinh_anh: true,
      },
    });

    if (savedImage) {
      res.status(200).send(savedImage);
    } else {
      res.send("Ảnh chưa được lưu!");
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const getListSavedImageByUserId = async (req, res) => {
  let { token } = req.headers;
  let decode = decodeToken(token);
  let { nguoi_dung_id } = decode.data.data;

  try {
    let savedImageList = await prisma.luu_anh.findMany({
      where: {
        nguoi_dung_id,
      },
      include: {
        hinh_anh: true,
      },
    });
    res.status(200).send(savedImageList);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export { getListSavedImageById, getListSavedImageByUserId };
