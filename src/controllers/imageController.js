import { PrismaClient } from "@prisma/client";
import compress_images from "compress-images";
import { decodeToken } from "../config/jwt.js";

const prisma = new PrismaClient();

const getImage = async (req, res) => {
  try {
    let listImage = await prisma.hinh_anh.findMany();
    res.status(200).send(listImage);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const getListImageByName = async (req, res) => {
  let { ten_hinh } = req.query;

  try {
    let listImage = await prisma.hinh_anh.findMany({
      where: {
        ten_hinh: {
          contains: ten_hinh,
        },
      },
    });
    res.status(200).send(listImage);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const getItemImageById = async (req, res) => {
  let { hinh_id } = req.params;

  try {
    let image = await prisma.hinh_anh.findFirst({
      where: {
        hinh_id: Number(hinh_id),
      },
      include: {
        nguoi_dung: true,
      },
    });
    res.status(200).send(image);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const getListImageByUserId = async (req, res) => {
  let { token } = req.headers;
  let decode = decodeToken(token);
  let { nguoi_dung_id } = decode.data.data;

  try {
    let listImage = await prisma.hinh_anh.findMany({
      where: {
        nguoi_dung_id,
      },
    });
    res.status(200).send(listImage);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const deleteImage = async (req, res) => {
  let { hinh_id } = req.params;
  try {
    await prisma.hinh_anh.delete({
      where: {
        hinh_id: Number(hinh_id),
      },
    });
    res.send("Delete thành công!");
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const uploadImage = async (req, res) => {
  let file = req.file;

  try {
    compress_images(
      process.cwd() + "/public/img/" + file.filename,
      process.cwd() + "/public/file/",
      { compress_force: false, statistic: true, autoupdate: true },
      false,
      { jpg: { engine: "mozjpeg", command: ["-quality", "50"] } },
      { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
      { svg: { engine: "svgo", command: "--multipass" } },
      {
        gif: {
          engine: "gifsicle",
          command: ["--colors", "64", "--use-col=web"],
        },
      },
      function (error, completed, statistic) {
        console.log("-------------");
        console.log(error);
        console.log(completed);
        console.log(statistic);
        console.log("-------------");
      }
    );

    res.status(201).send(file.filename);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const createImage = async (req, res) => {
  let { token } = req.headers;
  let decode = decodeToken(token);
  let { nguoi_dung_id } = decode.data.data;

  try {
    let infoUser = await prisma.nguoi_dung.findFirst({
      where: {
        nguoi_dung_id,
      },
    });

    if (infoUser) {
      let { ten_hinh, duong_dan, mo_ta } = req.body;
      let image = { ten_hinh, duong_dan, mo_ta, nguoi_dung_id };
      await prisma.hinh_anh.create({ data: image });
    } else {
      res.send("Không tồn tại user!");
      return;
    }
    res.status(201).send("Create thành công!");
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export {
  createImage,
  deleteImage,
  getImage,
  getItemImageById,
  getListImageByName,
  getListImageByUserId,
  uploadImage
};