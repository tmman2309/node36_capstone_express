import { PrismaClient } from "@prisma/client";
import { decodeToken } from "../config/jwt.js";
const prisma = new PrismaClient();

const getListCommentByImageId = async (req, res) => {
  let { hinh_id } = req.query;

  try {
    let listComment = await prisma.binh_luan.findMany({
      where: {
        hinh_id: Number(hinh_id),
      },
    });
    res.status(200).send(listComment);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const postComment = async (req, res) => {
  let { token } = req.headers;
  let decode = decodeToken(token);
  let { nguoi_dung_id } = decode.data.data;
  let { hinh_id, noi_dung } = req.body;

  try {
    let imageInfo = prisma.hinh_anh.findFirst({
      where: {
        hinh_id,
      },
    });

    if (!imageInfo) {
      res.send("Something went wrong!");
    } else {
      let newComment = {
        nguoi_dung_id,
        hinh_id: Number(hinh_id),
        ngay_binh_luan: new Date(),
        noi_dung,
      };

      await prisma.binh_luan.create({ data: newComment });
      res.status(201).send("Bình luận thành công!");
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export { getListCommentByImageId, postComment };

