import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
import { createToken } from "../config/jwt.js";
const prisma = new PrismaClient();

const login = async (req, res) => {
  let { email, mat_khau } = req.body;

  try {
    let checkEmail = await prisma.nguoi_dung.findFirst({
      where: {
        email,
      },
    });

    if (checkEmail) {
      let checkPassword = bcrypt.compareSync(mat_khau, checkEmail.mat_khau);
      if (checkPassword) {
        let token = createToken({ data: checkEmail });
        res.send(token);
        return;
      } else {
        res.send("Password không đúng!");
        return;
      }
    } else {
      res.status(400).send("Email hoặc password không đúng");
    }

    res.status(201).send("Login thành công");
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const signUp = async (req, res) => {
  let { email, mat_khau, ho_ten, tuoi, anh_dai_dien } = req.body;

  try {
    let data = await prisma.nguoi_dung.findFirst({
      where: {
        email,
      },
    });

    let hashPassword = bcrypt.hashSync(mat_khau, 10);
    if (!data) {
      let newData = {
        email,
        mat_khau: hashPassword,
        ho_ten,
        tuoi,
        anh_dai_dien,
      };

      await prisma.nguoi_dung.create({
        data: newData,
      });
      res.status(201).send("Đăng ký thành công!");
      return;
    } else {
      res.send("Email đã tồn tại!");
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export { login, signUp };
