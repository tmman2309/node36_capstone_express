import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
import { decodeToken } from "../config/jwt.js";

const prisma = new PrismaClient();

const getUserItem = async (req, res) => {
  let { email } = req.params;
  try {
    let user = await prisma.nguoi_dung.findFirst({
      where: {
        email,
      },
    });
    res.status(200).send(user);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

const updateUser = async (req, res) => {
  let { email, mat_khau, ho_ten, tuoi, anh_dai_dien } = req.body;
  let { token } = req.headers;
  let decode = decodeToken(token);
  let { nguoi_dung_id } = decode.data.data;

  try {
    let infoUser = await prisma.nguoi_dung.findFirst({
      where: {
        nguoi_dung_id,
      },
    });

    let hashPassword;
    if (mat_khau) hashPassword = bcrypt.hashSync(mat_khau, 10);

    if (infoUser) {
      let updateData = {
        ...infoUser,
        ...(email && { email }),
        ...(mat_khau && { mat_khau: hashPassword }),
        ...(ho_ten && { ho_ten }),
        ...(tuoi && { tuoi }),
        ...(anh_dai_dien && { anh_dai_dien }),
      };
      await prisma.nguoi_dung.update({
        data: updateData,
        where: {
          nguoi_dung_id,
        },
      });
    } else {
      res.send("Không tồn tại user!");
      return;
    }

    res.status(201).send("Update thành công!");
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export { getUserItem, updateUser };

