import express from "express";
import { khoaApi } from "../config/jwt.js";
import {
  createImage,
  deleteImage,
  getImage,
  getItemImageById,
  getListImageByName,
  getListImageByUserId,
  uploadImage,
} from "../controllers/imageController.js";
import { upload } from "../controllers/uploadController.js";

const imageRoutes = express.Router();

imageRoutes.get("/get-all", khoaApi, getImage);
imageRoutes.get("/get-list-by-name", khoaApi, getListImageByName);
imageRoutes.get("/get-item-by-id/:hinh_id", khoaApi, getItemImageById);
imageRoutes.get("/get-list-by-user-id", khoaApi, getListImageByUserId);
imageRoutes.delete("/delete/:hinh_id", khoaApi, deleteImage);
imageRoutes.post("/delete/:hinh_id", khoaApi, deleteImage);
imageRoutes.post("/upload", upload.single("file"), khoaApi, uploadImage);
imageRoutes.post("/create", khoaApi, createImage);

export default imageRoutes;
