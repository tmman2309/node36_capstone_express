import express from "express";
import { khoaApi } from "../config/jwt.js";
import {
  getListSavedImageById,
  getListSavedImageByUserId,
} from "../controllers/savedImageController.js";

const savedImageRoutes = express.Router();

savedImageRoutes.get(
  "/get-list-saved-image-by-id/:hinh_id",
  khoaApi,
  getListSavedImageById
);
savedImageRoutes.get(
  "/get-list-saved-image-by-user-id",
  khoaApi,
  getListSavedImageByUserId
);

export default savedImageRoutes;
