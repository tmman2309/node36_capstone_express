import express from "express";
import { khoaApi } from "../config/jwt.js";
import {
  getListCommentByImageId,
  postComment,
} from "../controllers/commentController.js";

const commentRoutes = express.Router();

commentRoutes.get("/get-list-by-image-id", khoaApi, getListCommentByImageId);
commentRoutes.post("/post", khoaApi, postComment);

export default commentRoutes;
