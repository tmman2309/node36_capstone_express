import express from "express";
import { khoaApi } from "../config/jwt.js";
import { getUserItem, updateUser } from "../controllers/userController.js";
// import { upload } from "../controllers/uploadController.js";

const userRoutes = express.Router();

userRoutes.get("/get-item/:email", khoaApi, getUserItem);
userRoutes.put("/update", khoaApi, updateUser);
// userRoutes.put("/update-avatar", upload.single("file"), khoaApi, uploadAvatar); //nếu muốn truyền nhiều hình thì upload.array("files", uploadAvatar)

export default userRoutes;
