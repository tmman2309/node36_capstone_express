import express from "express";
import authRoutes from "./authRoutes.js";
import commentRoutes from "./commentRoutes.js";
import imageRoutes from "./imageRoutes.js";
import savedImageRoutes from "./savedImageRoutes.js";
import userRoutes from "./userRoutes.js";

const rootRoutes = express.Router();
rootRoutes.use("/auth", authRoutes);
rootRoutes.use("/user", userRoutes);
rootRoutes.use("/image", imageRoutes);
rootRoutes.use("/comment", commentRoutes);
rootRoutes.use("/saved-image", savedImageRoutes);

export default rootRoutes;
